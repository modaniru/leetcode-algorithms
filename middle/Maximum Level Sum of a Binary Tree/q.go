package main

type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
}

func maxLevelSum(root *TreeNode) int {
	minLevel := 1
	maxValue := root.Val
	queue := make([]*TreeNode, 0)
	currentLevel := 1
	queue = append(queue, root)
	for len(queue) != 0{
		tempQueue := make([]*TreeNode, 0)
		levelSum := 0
		for len(queue) != 0{
			r := queue[0]
			queue = queue[1:]
			levelSum+=r.Val
			if r.Left != nil{
				tempQueue = append(tempQueue, r.Left)
			}
			if r.Right != nil{
				tempQueue = append(tempQueue, r.Right)
			}
		}
		if levelSum > maxValue {
			minLevel = currentLevel
			maxValue = levelSum
		}
		queue = tempQueue
		currentLevel++
	}
	return minLevel
}