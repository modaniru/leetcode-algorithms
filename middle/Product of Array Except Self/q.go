package main

func productExceptSelf(nums []int) []int {
	prefix := make([]int, len(nums))
	prefix[0] = nums[0]
	for i := 1; i < len(nums); i++{
		prefix[i] = nums[i] * prefix[i - 1]
	}
	postfix := nums[len(nums) - 1]
	prefix[len(prefix) - 1] = prefix[len(prefix) - 2]
	for i := len(nums) - 2; i > 0; i--{
		prefix[i] = prefix[i - 1] * nums[i + 1]
		postfix *= nums[i]
	}
	prefix[0] = postfix
	return prefix
}