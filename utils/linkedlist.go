package utils

import "strconv"

type ListNode struct {
	Val  int
	Next *ListNode
}

func Create(values ...int) *ListNode {
	if len(values) == 0 {
		return nil
	}
	head := &ListNode{Val: values[0], Next: nil}
	temp := head
	for i := 1; i < len(values); i++ {
		temp.Next = &ListNode{Val: values[i], Next: nil}
		temp = temp.Next
	}
	return head
}

func (ln *ListNode) GetString() string {
	res := ""
	for ln.Next != nil {
		res += "(" + strconv.Itoa(ln.Val) + ") -> "
		ln = ln.Next
	}
	res += "(" + strconv.Itoa(ln.Val) + ")"
	return res
}
