package utils

type TreeNode struct {
	Val   int
	Right *TreeNode
	Left  *TreeNode
}
