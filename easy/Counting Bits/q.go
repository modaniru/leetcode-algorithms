package main

import "fmt"

func countBits(n int) []int {
	res := make([]int, n+1)
	for i := 0; i <= n; i++ {
		count := 0
		value := i
		for value != 0 {
			count += value % 2
			value /= 2
		}
		res[i] = count
	}
	return res
}

func main(){
	fmt.Println(countBits(5))
	fmt.Println(countBits(0))
	fmt.Println(countBits(1))
}
