package main

func findMaxConsecutiveOnes(nums []int) int {
	count := 0
	maxCount := count
	for _, v := range nums{
		if v == 1{
			count++
		} else {
			maxCount = MaxInt(maxCount, count)
			count = 0
		}
	}
	maxCount = MaxInt(maxCount, count)
	return maxCount
}

func MaxInt(a, b int) int{
	if a > b{
		return a
	}
	return b
}