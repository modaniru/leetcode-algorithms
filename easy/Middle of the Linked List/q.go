package main

import (
	"algorithms/utils"
	"fmt"
)


func middleNode(head *utils.ListNode) *utils.ListNode {
    secondPointer := head
	count := 0
	for secondPointer != nil{
		if count % 2 == 1{
			head = head.Next
		}
		secondPointer = secondPointer.Next
		count++
	}
	return head
}


func main(){
	fmt.Printf("%v", *middleNode(utils.Create(1,2,3,4,5,6)))
	fmt.Printf("%v", *middleNode(utils.Create(1,2,3,4,5)))
	fmt.Printf("%v", *middleNode(utils.Create(1)))
	fmt.Printf("%v", *middleNode(utils.Create(1,2)))
}