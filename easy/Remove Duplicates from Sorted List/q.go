package main

type ListNode struct {
    Val int
    Next *ListNode
}

func deleteDuplicates(head *ListNode) *ListNode {
	if head == nil{
		return nil
	}
	tempHead := head
	temp := head
	for temp != nil{
		if tempHead.Val != temp.Val{
			tempHead.Next = temp
			tempHead = tempHead.Next
		}
		temp = temp.Next
	}
	tempHead.Next = nil
	return head
}