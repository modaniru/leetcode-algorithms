package main

func validMountainArray(arr []int) bool {
	if len(arr) < 3{
		return false
	}
	i := 0
  	for arr[i] < arr[i+1]{
		i++
		if i + 1 == len(arr){
			return false
		}
	}
	if i == 0{
		return false
	}
	for ; i < len(arr) - 1; i++{
		if arr[i] <= arr[i+1]{
			return false
		}
	}
	return true
}