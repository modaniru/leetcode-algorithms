package main

func numJewelsInStones(jewels string, stones string) int {
	m := make(map[rune]bool)
	for _, r := range jewels{
		m[r] = true
	}
	count := 0
	for _, s := range stones{
		if m[s]{
			count++
		}
	}
	return count
}