package main

import "fmt"

func generate(numRows int) [][]int {
    res := [][]int{{1}}
	for i := 1; i < numRows; i++{
		temp := []int{1}
		for j := 0; j < len(res) - 1; j++{
			temp = append(temp, res[i-1][j] + res[i-1][j + 1])
		}
		temp = append(temp, 1)
		res = append(res, temp)
	}
	return res
}

func main(){
	fmt.Println(generate(1))
}