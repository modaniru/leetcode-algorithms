package main

import "fmt"

func containsDuplicate(nums []int) bool {
    set := make(map[int]bool)
	for _, value := range nums {
		v := set[value]
		if v {
			return true
		}
		set[value] = true
	}
	return false
}

func main(){
	fmt.Println(containsDuplicate([]int{1,2,3,4,5,6}))
	fmt.Println(containsDuplicate([]int{1}))
	fmt.Println(containsDuplicate([]int{1,1,1,1,1,1}))
	fmt.Println(containsDuplicate([]int{1,2,3,4,5,6,6}))
}