package main

import "strings"

func reverseWords(s string) string {
    words := strings.Split(s, " ")
	for i := range words{
		words[i] = reverseWord(words[i])
	}
	return strings.Join(words, " ")
}

func reverseWord(word string) string{
	runes := []rune(word)
	for left, right := 0, len(runes) - 1; left < right;{
		runes[left], runes[right] = runes[right], runes[left]
		left++
		right--
	}
	return string(runes)
}