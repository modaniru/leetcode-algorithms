package main

func isSubsequence(s string, t string) bool {
	if len(s) == 0{
		return false
	}
    subI := 0
	for _, v := range t{
		if v == []rune(s)[subI]{
			subI++
			if subI == len(s){
				return true
			}
		}
	}
	return false
}