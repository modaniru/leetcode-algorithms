package main

func majorityElement(nums []int) int {
    m := make(map[int]int)
	for _, v := range(nums){
		m[v]++
	}
	res := 0
	count := 0
	for k, v := range(m){
		if v > count{
			res = k
			count = v 
		}
	}
	return res
}