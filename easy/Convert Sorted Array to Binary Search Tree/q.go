package main

type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
 }
 

func sortedArrayToBST(nums []int) *TreeNode {
    if len(nums) == 0{
		return nil
	}
	middle := len(nums) / 2
	res := &TreeNode{
		Val: nums[middle],
		Right: sortedArrayToBST(nums[middle + 1:]),
		Left: sortedArrayToBST(nums[:middle]),
	}
	return res
}