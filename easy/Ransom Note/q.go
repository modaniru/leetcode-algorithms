package main

func canConstruct(ransomNote string, magazine string) bool {
	m := make(map[rune]int)
	for _, r := range magazine{
		m[r]++
	}
	for _, r := range ransomNote{
		_, ok := m[r]
		if !ok{
			return false
		}
		m[r]--
		if m[r] == 0{
			delete(m, r)
		}
	}
	return true
}