package main

import "strconv"

type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
 }
 
func binaryTreePaths(root *TreeNode) []string {
	if root == nil {
		return []string{}
	}
	if root.Left == nil && root.Right == nil{
		return []string{strconv.Itoa(root.Val)}
	}
	res := binaryTreePaths(root.Left)
	res = append(res, binaryTreePaths(root.Right)...)
	for i := range(res){
		res[i] = strconv.Itoa(root.Val) + "->" + res[i]
	}
	return res
}
