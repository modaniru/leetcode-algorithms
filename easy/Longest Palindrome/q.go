package main

func longestPalindrome(s string) int {
	mapa := make(map[rune]int)
	for _, r := range s{
		mapa[r]++
	}
	count := 0
	hasOdd := false
	for _, c := range(mapa){
		if c % 2 == 1 && !hasOdd{
			count += c
			hasOdd = true
		}
		count += c - (c % 2)
	}
	return count
}