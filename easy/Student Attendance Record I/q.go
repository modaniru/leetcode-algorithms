package main

import "fmt"

func checkRecord(s string) bool {
	miss := 0
	late := 0
	for _, v := range s {
		if v == 'A' {
			miss++
			//if late is not 2 is should be clear?
			late = 0
			if miss == 2 {
				return false
			}
		} else if v == 'L' {
			late++
			if late == 3 {
				return false
			}
		} else {
			late = 0
		}
	}
	return true
}

func main() {
	fmt.Println(checkRecord("PPALLP"))
	fmt.Println(checkRecord("PPAALLP"))
	fmt.Println(checkRecord("PPALLLP"))
	fmt.Println(checkRecord("PPALALLLP"))
}
