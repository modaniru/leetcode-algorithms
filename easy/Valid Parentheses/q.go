package main

func isValid(s string) bool {
	equals := func (a, b rune) bool{
		if a == '[' && b == ']'{
			return true
		} else if a == '(' && b == ')'{
			return true
		} else if a == '{' && b == '}'{
			return true
		}
		return false
	}
	stack := []rune{}
	for _, r := range s{
		if r == '(' || r == '{' || r == '['{
			stack = append(stack, r)
			continue
		}
		if len(stack) == 0 || !equals(stack[len(stack) - 1], r){
			return false
		}
		stack = stack[:len(stack) - 1]
	}
	return len(stack) == 0
}