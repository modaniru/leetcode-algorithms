package main

func isMonotonic(nums []int) bool {
	down := func(a, b int) bool{
		return a >= b
	}
	up := func(a, b int) bool{
		return a <= b
	}
	now := up
	if len(nums) == 1{
		return true
	}
	index := 1
	for index < len(nums) && nums[index] == nums[index - 1]{
		index++
	}
	if nums[index] < nums[index-1]{
		now = down
	}
	for ; index < len(nums); index++{
		if !now(nums[index - 1], nums[index]){
			return false
		}
	}
	return true
}