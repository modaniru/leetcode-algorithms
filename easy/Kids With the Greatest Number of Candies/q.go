package main

func kidsWithCandies(candies []int, extraCandies int) []bool {
    max := candies[0]
	for i := 1; i < len(candies); i++{
		if candies[i] > max{
			max = candies[i]
		}
	}
	res := []bool{}
	for _, v := range(candies){
		res = append(res, v + extraCandies >= max)
	}
	return res
}