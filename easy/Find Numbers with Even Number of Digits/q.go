package main

func findNumbers(nums []int) int {
	count := 0
    for _, v := range nums{
		if isChet(v){
			count++
		}
	}
	return count
}

func isChet(num int) bool{
	count := 1
	for num >= 10{
		count++
		num /= 10
	}
	return count % 2 == 0
}