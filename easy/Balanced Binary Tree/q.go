package main

import "math"

type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
}

func isBalanced(root *TreeNode) bool {
	res, _ := isBalanced2(root)
	return res
}


func isBalanced2(root *TreeNode) (bool, int) {
	if root == nil{
		return true, 0
	}
	rightB, depthR := isBalanced2(root.Right)
	leftB, depthL := isBalanced2(root.Left)
	if depthR < depthL{
		depthR = depthL
	}
	a := math.Abs(float64(depthR) - float64(depthL))
	return rightB && leftB && a <= 1, depthR + 1
}
