package main

func backspaceCompare(s string, t string) bool {
    return do(s) == do(t)
}

func do(s string) string{
	stack := []rune{}
	for _, r := range(s){
		if r == '#'{
			if len(stack) != 0{
				stack = stack[:len(stack) - 1]
			}
			continue
		}
		stack = append(stack, r)
	}
	return string(stack)
}