package main

import "fmt"

func search(nums []int, target int) int {
    i, j := 0, len(nums)
	for i != j{
		mid := (i + j) / 2
		if(nums[mid] > target){
			j = mid
		} else if (nums[mid] < target){
			i = mid + 1
		} else {
			return mid
		}
	}
	if i < len(nums) && nums[i] == target{
		return i
	}
	return -1
}

func main(){
	fmt.Println(search([]int{1,2,3,4,5,6}, 6))
}