package main

func sortArrayByParity(nums []int) []int {
    left := 0
	right := len(nums) - 1
	for right > left{
		if nums[right] % 2 == 1{
			right--
		} else if nums[left] % 2 == 0{
			left++
		} else {
			nums[left], nums[right] = nums[right], nums[left]
			left++
			right--
		}
	}
	return nums
}