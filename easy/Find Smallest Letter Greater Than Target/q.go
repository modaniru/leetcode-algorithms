package main

func nextGreatestLetter(letters []byte, target byte) byte {
    index := 0
	for i := range(letters){
		if letters[i] > target{
			index = i
			break
		}
	}
	return letters[index]
}