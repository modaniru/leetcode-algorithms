package main

type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
}

func findTarget(root *TreeNode, k int) bool {
	return findTargetWithMap(root, k, make(map[int]bool))
}

func findTargetWithMap(root *TreeNode, k int, m map[int]bool) bool{
	if root == nil{
		return false
	}
	v := m[k - root.Val]
	m[root.Val] = true 
	return v || findTargetWithMap(root.Left, k, m) || findTargetWithMap(root.Right, k, m)
}