package main

func removeElement(nums []int, val int) int {
	left := 0
	right := len(nums) - 1
	ln := len(nums)
	for left <= right{
		if nums[right] == val{
			right--
			ln--
			continue
		}
		if nums[left] != val{
			left++
			continue
		}
		nums[left], nums[right] = nums[right], nums[left]
		left++
		right--
		ln--
	}
	return ln
}