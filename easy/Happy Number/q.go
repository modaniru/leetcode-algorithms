package main

import "fmt"

func isHappy(n int) bool {
	cycle := make(map[int]bool)
	for n != 1{
		cycle[n] = true
		fmt.Println(n)
		new := 0
		for n != 0{
			digit := n % 10
			new += digit * digit
			n /= 10
		}
		if cycle[new]{
			return false
		}
		n = new
	}
	return true
}

func main(){
	isHappy(2)
}