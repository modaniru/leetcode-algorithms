package main

 func guessNumber(n int) int {
    low, up := 1, n
    for low <= up{
        mid := (low + up) / 2
        res := guess(mid)
        if res == 0{
            return mid
        } else if (res < 0){
            up = mid - 1
        } else {
            low = mid + 1
        }
    }
    return low
}
//leetcode func 
func guess(num int) int{
	return 0
}