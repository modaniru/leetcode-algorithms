package main

import "algorithms/utils"

//remove 'utils.'
func postorderTraversal(root *utils.TreeNode) []int {
    if root == nil {
		return []int{}
	}
	res := append(postorderTraversal(root.Left), postorderTraversal(root.Right)...)
	return append(res, root.Val)
}