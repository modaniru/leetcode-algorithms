package main

func findDisappearedNumbers(nums []int) []int {
	m := make(map[int]bool)
	for i := 1; i <= len(nums); i++{
		m[i] = true
	}
	for _, v := range nums{
		if m[v] {
			delete(m, v)
		}
	}
	res := []int{}
	for k := range m{
		res = append(res, k)
	}
	return res
}