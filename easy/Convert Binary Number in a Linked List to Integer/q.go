package main


type ListNode struct {
    Val int
    Next *ListNode
}

func getDecimalValue(head *ListNode) int {
	res := head.Val
	head = head.Next
	for head != nil{
		res = res << 1
		res += head.Val
		head = head.Next
	}
	return res
}