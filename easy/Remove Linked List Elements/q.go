package main

import (
	"algorithms/utils"
	"fmt"
)

// remove utils for LeetCode
// O(n)
func removeElements(head *utils.ListNode, val int) *utils.ListNode {
	//todo короче еще лучше делали
	//без первой и вроторой проверки
	//создали временную ноду, и ее прогоняли через цикл
	//Возвращали head.next
	if head == nil {
		return head
	}
	temp := head
	for temp.Next != nil {
		if temp.Next.Val == val {
			temp.Next = temp.Next.Next
		} else {
			temp = temp.Next
		}
	}
	if head.Val == val{
		return head.Next
	}
	return head
}

func main() {
	list := utils.Create(6, 6, 3, 4, 5, 6, 6, 6)
	fmt.Println(list.GetString())
	list = removeElements(list, 6)
	fmt.Println(list.GetString())
}
