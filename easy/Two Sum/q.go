package main

func twoSum(nums []int, target int) []int {
	elIndex := make(map[int]int)
	for i, v := range nums{
		index, ok := elIndex[target - v]
		if ok{
			return []int{index, i}
		}
		elIndex[v] = i
	}
	return []int{-1, -1}
}