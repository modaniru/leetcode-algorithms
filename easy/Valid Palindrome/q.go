package main

import "strings"

func isPalindrome(s string) bool {
    runes := []rune(strings.ToLower(s))
	for left, right := 0, len(runes) - 1; left < right;{
		if !checkRune(runes[left]){
			left++
			continue
		} else if !checkRune(runes[right]){
			right--
			continue
		} else if runes[left] != runes[right]{
			return false
		}
		left++
		right--
	}
	return true
}

func checkRune(r rune) bool{
	return r >= 'a' && r <= 'z' || r >= 'A' && r <= 'Z' || r >= '0' && r <= '9'
}

