package main

import "fmt"

func plusOne(digits []int) []int {
	mind := 1
    for i := len(digits) - 1; i >= 0; i--{
		num := digits[i] + mind
		digits[i] = num % 10
		if num >= 10 {
			mind = 1
		} else {
			mind = 0
			break
		}
	}
	if mind == 1 {
		return append([]int{1}, digits...)
	}
	return digits
}

func main(){
	fmt.Println(plusOne([]int{9}))
	fmt.Println(plusOne([]int{1}))
	fmt.Println(plusOne([]int{1,9}))
	fmt.Println(plusOne([]int{9,9,9,9}))
}