package main

func isPowerOfFour(n int) bool {
    for n > 1 && n == n / 4 * 4{
		n /= 4
	}
	return n == 1
}