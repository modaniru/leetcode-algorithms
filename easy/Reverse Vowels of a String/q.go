package main

func reverseVowels(s string) string {
	left := 0
	  right := len(s) - 1
	  arr := []rune(s)
	  for left < right{
		  if !checkLetter(arr[left]){
			  left++
			  continue
		  }
		  if !checkLetter(arr[right]){
			  right--
			  continue
		  }
		  arr[left], arr[right] = arr[right], arr[left]
		  left++
		  right--
	  }
	  return string(arr)
  }
  
  func checkLetter(b rune) bool{
	  if b > 'Z'{
		  b = b - 'a' + 'A'
	  }
	  return b == 'A' || b == 'E' || b == 'I' || b == 'O' || b == 'U' 
  }