package main

func intersect(nums1 []int, nums2 []int) []int {
	m := make(map[int]int)
	for _, i := range nums1{
		m[i]++
	}
	res := []int{}
	for _, i := range nums2{
		count, ok := m[i]
		if ok{
			res = append(res, i)
			count--
			m[i] = count
			if count == 0{
				delete(m, i)
			}
		}
	}
	return res
}