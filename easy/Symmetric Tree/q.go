package main

import (
	"algorithms/utils"
)

func isSymmetric(root *utils.TreeNode) bool {
	return isSym(root.Left, root.Right)
}

func isSym(left *utils.TreeNode, right *utils.TreeNode) bool {
	if left == nil && right == nil {
		return true
	}
	if right == nil || left == nil || left.Val != right.Val {
		return false
	}
	return isSym(left.Left, right.Right) && isSym(left.Right, right.Left)
}
