package main

func sortedSquares(nums []int) []int {
	left := 0
	right := len(nums) - 1
	res := make([]int, len(nums))
	index := len(nums) - 1
	for left <= right{
		rs := nums[right] * nums[right]
		ls := nums[left] * nums[left]
		if ls < rs{
			res[index] = rs
			right--
		} else {
			res[index] = ls
			left++
		}
		index--
	}
	return res
}