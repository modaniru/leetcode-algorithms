package main

func mySqrt(x int) int {
	if x < 2{
		return x
	}
	right := x / 2 + 1
	left := 0
	for left < right{
		mid := (left + right) / 2
		val := mid * mid
		if val == x{
			return mid
		} else if val > x {
			right = mid
		} else {
			left = mid
		}
	}
	return right - 1
}