package main

import "fmt"

func isPalindrome(x int) bool {
	if x < 0  {
		return false
	}
	var tmp int = x
	var res int = 0
	for ; tmp != 0; tmp/=10{
		num := tmp % 10
		res += num
		res *= 10
	}
	res /= 10
	fmt.Println(res)
	return res == x
}

func main(){
	fmt.Println(isPalindrome(121))
}