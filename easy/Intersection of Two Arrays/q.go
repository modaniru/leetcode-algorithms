package main

func intersection(nums1 []int, nums2 []int) []int {
    mapa := make(map[int]bool)
	for _, v := range nums1 {
		mapa[v] = true
	}
	res := []int{}
	for _, v := range nums2{
		if mapa[v]{
			res = append(res, v)
			delete(mapa, v)
		}
	}
	return res
}