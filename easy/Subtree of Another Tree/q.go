package main

type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
}

func isSubtree(root *TreeNode, subRoot *TreeNode) bool {
	if root == nil {
		return false
	}
	if checkRoots(root, subRoot){
		return true
	}
	return isSubtree(root.Left, subRoot) || isSubtree(root.Right, subRoot)
}

func checkRoots(root *TreeNode, subRoot *TreeNode) bool{
	if subRoot == nil && root == nil{
		return true
	}
	if root == nil || subRoot == nil || root.Val != subRoot.Val{
		return false
	}
	return checkRoots(root.Left, subRoot.Left) && checkRoots(root.Right, subRoot.Right) 
}

func main(){
	root := &TreeNode{3, &TreeNode{4, &TreeNode{1, &TreeNode{0, nil, nil}, nil}, &TreeNode{2, nil,nil}}, &TreeNode{5, nil, nil}}
	sub := &TreeNode{4, &TreeNode{1, nil, nil}, &TreeNode{2, nil,nil}}
	isSubtree(root, sub)
}