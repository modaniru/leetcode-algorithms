package main

import "math"

func hammingDistance(x int, y int) int {
	len := 0
    for i := 0; i < 31; i++{
		bit := int(math.Pow(2, float64(i)))
		if x & bit != y & bit{
			len++
		}
	}
	return len
}