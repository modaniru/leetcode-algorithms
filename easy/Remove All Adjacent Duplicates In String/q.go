package main

func removeDuplicates(s string) string {
	stack := []byte{}
	stack = append(stack, s[0])
	for i := 1; i < len(s); i++{
		if len(stack) != 0 && s[i] == stack[len(stack) - 1]{
			stack = stack[:len(stack)-1]
			continue
		}
		stack = append(stack, s[i])
	}
	return string(stack)
}