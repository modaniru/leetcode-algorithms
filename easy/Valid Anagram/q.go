package main

func isAnagram(s string, t string) bool {
	if len(s) != len(t) {
		return false
	}
	set := make(map[byte]int)
	for i := 0; i < len(s); i++{
		set[s[i]]++
		if set[s[i]] == 0{
			delete(set, s[i])
		}
		set[t[i]]--
		if set[t[i]] == 0{
			delete(set, t[i])
		}
	}
	return len(set) == 0
}