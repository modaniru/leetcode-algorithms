package main

func findLengthOfLCIS(nums []int) int {
	count := 1
	maxCount := count
	for i := 1; i < len(nums); i++{
		if nums[i] > nums[i-1]{
			count++
		} else {
			if count > maxCount{
				maxCount = count
			}
			count = 1
		}
	}
	if count > maxCount{
		maxCount = count
	}
	return maxCount
}