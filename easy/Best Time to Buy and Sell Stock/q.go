package main

func maxProfit(prices []int) int {
	minPrice := prices[0]
	maxPrice := prices[0]
	maxDifference := 0
	for i := 1; i < len(prices); i++{
		if prices[i] > maxPrice{
			maxPrice = prices[i]
		}
		if prices[i] < minPrice{
			diff := maxPrice - minPrice
			if diff > maxDifference{
				maxDifference = diff
			}
			maxPrice = prices[i]
			minPrice = prices[i]
		}
	}
	diff := maxPrice - minPrice
	if diff > maxDifference{
		return diff
	}
	return maxDifference
}

/*
6 4 5 1 2 3 4 2 3 0 8
*/