package main

func longestCommonPrefix(strs []string) string {
    prefix := strs[0]
	for i := 1; i < len(strs); i++{
		end := 0
		for end < len(prefix) && end < len(strs[i]) && prefix[end] == strs[i][end]{
			end++
		}
		prefix = prefix[:end + 1]
	}
	return prefix
}