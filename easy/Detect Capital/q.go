package main

func detectCapitalUse(word string) bool {
	check := true
	for _, r := range word{
		if r > 'Z' || r < 'A'{
			check = false
			break
		}
	}
	if check{
		return true
	}
	check = true
	if word[0] >= 'A' && word[0] <= 'Z'{
		word = word[1:]
	}
	for _, r := range word{
		if r > 'z' || r < 'a'{
			check = false
			break
		}
	}
	return check
}