package main

import "sort"

func findContentChildren(g []int, s []int) int {
    sort.Ints(g)
	sort.Ints(s)
	count := 0
	c := 0
	k := 0
	for k < len(g) && c < len(s){
		if s[c] >= g[k]{
			count++
			c++
			k++
		} else {
			c++
		}
	}
	return count
}

func MinInt(a, b int) int{
	if a > b {
		return b
	}
	return a
}