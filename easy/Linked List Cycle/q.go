package main

import "algorithms/utils"

func hasCycle(head *utils.ListNode) bool {
	if head == nil {
		return false
	}
    fast := head.Next
	slow := head
	for fast != nil{
		if fast == slow{
			return true
		}
		slow = slow.Next
		fast = fast.Next
		if fast == nil{
			return false
		}
		fast = fast.Next
	}
	return false
}