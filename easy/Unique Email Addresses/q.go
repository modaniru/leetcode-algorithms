package main

import (
	"fmt"
	"strings"
)

func numUniqueEmails(emails []string) int {
	m := make(map[string]map[string]bool)
	count := 0
    for _, email := range emails{
		ld := strings.Split(email, "@")
		fp := strings.Split(ld[0], "+")[0]
		fp = strings.ReplaceAll(fp, ".", "")
		res := m[ld[1]][fp]
		if !res {
			mapa := m[ld[1]]
			if mapa == nil {
				m[ld[1]] = make(map[string]bool)
				mapa = m[ld[1]]
			}
			mapa[fp] = true
			count++
		}
	}
	return count
}

func main(){
	fmt.Println(numUniqueEmails([]string{"a@leetcode.com", "b@leetcode.com", "c@leetcode.com"}))
	fmt.Println(numUniqueEmails([]string{"test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"}))
}