package main

type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
}

func averageOfLevels(root *TreeNode) []float64 {
	thisLevel := []*TreeNode{root}
	nextLevel := []*TreeNode{}
	res := []float64{}
	for len(nextLevel) != 0 || len(thisLevel) != 0 {
		value := 0
		count := 0
		for len(thisLevel) != 0{
			r := thisLevel[0]
			thisLevel = thisLevel[1:]
			value += r.Val
			count++
			if r.Left != nil{
				nextLevel = append(nextLevel, r.Left)
			}
			if r.Right != nil{
				nextLevel = append(nextLevel, r.Right)
			}
		}
		res = append(res, float64(value) / float64(count))
		thisLevel = nextLevel
		nextLevel = nil
	}
	return res
}