package main

import "fmt"

func licenseKeyFormatting(s string, k int) string {
    key := ""
	ln := 0
	for i := len(s) - 1; i >= 0; i--{
		if s[i] == '-'{
			continue
		}
		rn := s[i]
		if rn > 'Z' && (rn > '9' || rn < '0'){
			rn = rn - 'z' + 'Z'
		}
		key = string(rn) + key
		ln++
		if ln == k{
			key = "-" + key
			ln = 0
		}
	}
	if len(key) != 0 && key[0] == '-'{
		return key[1:]
	}
	return key
}

func main(){
	fmt.Println(licenseKeyFormatting("5F3Z-2e-9-w", 4))
}