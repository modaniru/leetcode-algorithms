package main
 
type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
}
 
func sumOfLeftLeaves(root *TreeNode) int {
	if root.Left == nil && root.Right == nil{
		return 0
	}
	return alg(root)
}

func alg(root *TreeNode) int{
	if root == nil{
		return 0
	}
	if root.Left == nil && root.Right == nil{
		return root.Val
	}
	res := 0
	res += alg(root.Left)
	if root.Right == nil || (root.Right.Left == nil && root.Right.Right == nil){
		return res
	}
	res += alg(root.Right)
	return res
}