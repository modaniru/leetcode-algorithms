package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeTwoLists(list1 *ListNode, list2 *ListNode) *ListNode {
	if list1 == nil {
		return list2
	}
	if list2 == nil {
		return list1
	}
	res := &ListNode{Val: 0}
	temp := res
	for list1 != nil && list2 != nil {
		if list1.Val < list2.Val {
			temp.Next = &ListNode{Val: list1.Val}
			list1 = list1.Next
			temp = temp.Next
		} else {
			temp.Next = &ListNode{Val: list2.Val}
			list2 = list2.Next
			temp = temp.Next
		}
	}
	for list1 != nil {
		temp.Next = &ListNode{Val: list1.Val}
		list1 = list1.Next
		temp = temp.Next
	}
	for list2 != nil {
		temp.Next = &ListNode{Val: list2.Val}
		list2 = list2.Next
		temp = temp.Next
	}
	return res.Next
}
