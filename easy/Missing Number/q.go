package main

func missingNumber(nums []int) int {
    sum := int(((0 + float64(len(nums))) / 2.0) * (float64(len(nums)) + 1))
	for _, v := range(nums) {
		sum -= v
	}
	return sum
}

func main(){
	missingNumber([]int{0,1,2,3,4})
}