package main

func firstUniqChar(s string) int {
	m := make(map[rune]int)
	for i, r := range s{
		_, ok := m[r]
		if ok {
			m[r] = -1
		} else {
			m[r] = i
		}
	}
	min := len(s)
	for _, i := range m{
		if i != -1{
			min = MinInt(min, i)
		}
	}
	if min == len(s){
		return -1
	}
	return min
}

func MinInt(a, b int) int{
	if a > b{
		return b
	}
	return a
}