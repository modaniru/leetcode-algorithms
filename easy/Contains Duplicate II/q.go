package main


func containsNearbyDuplicate(nums []int, k int) bool {
	mapa := make(map[int]bool)
	for i := 0; i <= k; i++{
		if i == len(nums) {
			return false
		}
		v := mapa[nums[i]]
		if v{
			return true
		}
		mapa[nums[i]] = true
	}
	for i := k + 1; i < len(nums); i++{
		delete(mapa, nums[i - k - 1])
		v := mapa[nums[i]]
		if v {
			return true
		}
		mapa[nums[i]] = true
	}
	return false
}