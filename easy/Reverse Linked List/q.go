package main

import (
	"algorithms/utils"
	"fmt"
)

func reverseList(head *utils.ListNode) *utils.ListNode {
	res := new(utils.ListNode)
	res.Val = head.Val
	head = head.Next
    for head != nil{
		temp := new(utils.ListNode)
		temp.Val = head.Val
		temp.Next = res
		res = temp
		head = head.Next
	}
	return res
}

func main(){
	list := utils.Create(1,2,3,4,5)
	list = reverseList(list)
	fmt.Println(*list)
}