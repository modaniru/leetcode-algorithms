package main

import "fmt"

func tribonacci(n int) int {
	arr := append([]int{}, 0, 1, 1)
	for i := 3; i <= n; i++{
		arr = append(arr, arr[i-3] + arr[i-2] + arr[i-1])
	}
	return arr[n]
}

func main(){
	fmt.Println(tribonacci(4))
	fmt.Println(tribonacci(2))
	fmt.Println(tribonacci(25))
}