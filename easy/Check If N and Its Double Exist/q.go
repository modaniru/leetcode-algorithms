package main

import "fmt"

func checkIfExist(arr []int) bool {
	mapa := make(map[int]bool)
	for _, v := range arr {
		if mapa[v*2] || (mapa[v/2] && (v/2)*2 == v) {
			return true
		}
		mapa[v] = true
	}
	return false
}

func main() {
	fmt.Println(checkIfExist([]int{1, 43, 12, 5, 6}))
	fmt.Println(checkIfExist([]int{1, 43, 3, 5, 5}))
}
