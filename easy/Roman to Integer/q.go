package main

func romanToInt(s string) int {
	answ := 0
	for i := 0; i < len(s);{
		if s[i] == 'M'{
			answ += 1000
		} else if s[i] == 'D'{
			answ += 500
		} else if s[i] == 'C'{
			if i == len(s) - 1{
				answ += 100
			} else if s[i + 1] == 'M'{
				answ += 900
				i++
			} else if s[i + 1] == 'D'{
				answ += 400
				i++
			} else {
				answ += 100
			}
		} else if s[i] == 'L'{
			answ += 50
		} else if s[i] == 'X'{
			if i == len(s) - 1{
				answ += 10
			} else if s[i + 1] == 'L'{
				answ += 40
				i++
			} else if s[i + 1] == 'C'{
				answ += 90
				i++
			} else {
				answ += 10
			}
		} else if s[i] == 'V'{
			answ += 5
		} else if s[i] == 'I'{
			if i == len(s) - 1{
				answ += 1
			} else if s[i + 1] == 'V'{
				answ += 4
				i++
			} else if s[i + 1] == 'X'{
				answ += 9
				i++
			} else {
				answ += 1
			}
		}
		i++
	}
	return answ
}