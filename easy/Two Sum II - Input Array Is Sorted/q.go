package main

func twoSum(numbers []int, target int) []int {
    left := 0
	right := len(numbers) - 1
	for right > left{
		sum := numbers[right] + numbers[left]
		if sum == target{
			return []int{left+1, right+1}
		}
		if sum > target{
			right--
		} else {
			left++
		}
	}
	return []int{-1,-1}
}