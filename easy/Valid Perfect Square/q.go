package main

import "fmt"

func isPerfectSquare(num int) bool {
	for i := 1; i < num; i++ {
		if i*i >= num {
			return i*i == num
		}
	}
	return false
}

func main(){
	fmt.Println(isPerfectSquare(16))
	fmt.Println(isPerfectSquare(8))
}