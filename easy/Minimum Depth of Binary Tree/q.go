package main

import (
	"algorithms/utils"
	"math"
)

func minDepth(root *utils.TreeNode) int {
	if root == nil{
		return 0
	}
	if root.Left == nil && root.Right == nil{
		return 1
	}
	leftDepth := minDepth(root.Left)
	rightDepth := minDepth(root.Right)
	if leftDepth == 0{
		return rightDepth
	} else if rightDepth == 0{
		return leftDepth
	} else {
		return int(math.Min(float64(leftDepth), float64(rightDepth))) + 1
	}
}

