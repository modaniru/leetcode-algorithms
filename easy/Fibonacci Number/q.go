package main

func fib(n int) int {
	res := []int{0, 1}
	for i := 2; i <= n; i++{
		res = append(res, res[i-1] + res[i-2])
	}
	return res[n]
}