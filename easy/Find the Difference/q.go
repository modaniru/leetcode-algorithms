package main

func findTheDifference(s string, t string) byte {
	letters := make(map[byte]int)
	for i := 0; i < len(s); i++{
		letters[s[i]]++
		if letters[s[i]] == 0{
			delete(letters, s[i])
		}
		letters[t[i]]--
		if letters[t[i]] == 0{
			delete(letters, t[i])
		}
	}
	letters[t[len(t) - 1]]--
	if letters[t[len(t) - 1]] == 0{
		delete(letters, t[len(t) - 1])
	}
	for k, _ := range letters{
		return k
	}
	return 0
}