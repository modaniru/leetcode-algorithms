package main

func isIsomorphic(s string, t string) bool {
	m := make(map[rune]byte)
	used := make(map[byte]bool)
	for i, r := range s{
		val, ok := m[r]
		u := used[t[i]]
		if !ok && !u{
			m[r] = t[i]
			used[t[i]] = true
			continue
		}
		if val != t[i]{
			return false
		}
	}
	return true
}