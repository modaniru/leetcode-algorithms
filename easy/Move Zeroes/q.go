package main


func moveZeroes(nums []int) {
	leftPointer := 0
	rightPointer := 0
	for rightPointer < len(nums){
		if nums[rightPointer] == 0{
			rightPointer++
			continue
		}
		if nums[leftPointer] != 0{
			if leftPointer == rightPointer{
				rightPointer++
			}
			leftPointer++
			continue
		}
		nums[leftPointer], nums[rightPointer] = nums[rightPointer], nums[leftPointer]
	}
}

func main(){
	moveZeroes([]int{0,0,0,1,1,1,2,3,4,0,0,4,3,0,0})
}
