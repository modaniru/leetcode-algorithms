package main

func searchInsert(nums []int, target int) int {
	left := 0
    for right := len(nums); left < right;{
		middle := (left + right) / 2
		if target == nums[middle]{
			return middle
		} else if target < nums[middle]{
			right = middle
		} else {
			left = middle + 1
		}
	}
	if left == len(nums) || nums[left] > target{
		return left
	} 
	return left + 1
}

/*
	* * *
	0 2 4
	1
	*
	0
	
*/