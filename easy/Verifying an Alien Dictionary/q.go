package main

func isAlienSorted(words []string, order string) bool {
    dict := make(map[rune]int)
	for i, v := range(order){
		dict[v] = i
	}
	for i := 0; i < len(words) - 1; i++{
		if !compareWords(words[i], words[i+1], dict){
			return false
		}
	}
	return true
}

func compareWords(w1, w2 string, dict map[rune]int) bool{
	small := minInt(len(w1), len(w2))
	for i := 0; i < small; i++{
		r1 := rune(w1[i])
		r2 := rune(w2[i])
		if dict[r1] > dict[r2]{
			return false
		} else if dict[r1] < dict[r2]{
			return true
		}
	}
	return len(w1) <= len(w2)
}

func minInt(a, b int) int{
	if a < b {
		return a
	}
	return b
}