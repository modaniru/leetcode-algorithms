package main

import "fmt"

func flipAndInvertImage(image [][]int) [][]int {
    for _, a := range(image){
		left := 0
		right := len(a) - 1
		for left < right{
			a[left], a[right] = a[right] ^ 1, a[left] ^ 1
			left++
			right--
		} 
		if left == right{
			a[left] = a[left] ^ 1
		}
	}
	return image
}

func main(){
	value := [][]int{{0,1,1},{0,1,0},{0,0,0}}
	flipAndInvertImage(value)
	fmt.Println(value)
}