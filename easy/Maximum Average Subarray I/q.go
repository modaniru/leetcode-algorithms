package main

import (
	"fmt"
)

func findMaxAverage(nums []int, k int) float64 {
    maxSum := 0
	for i := 0; i < k; i++{
		maxSum += nums[i]
	}
	sum := maxSum
	for i := k; i < len(nums); i++{
		sum -= nums[i - k]
		sum += nums[i]
		if(sum > maxSum){
			maxSum = sum
		}
	}
	return float64(maxSum) / float64(k)
}

func main(){
	fmt.Println(findMaxAverage([]int{1,12,-5,-6,50,3}, 4))
}