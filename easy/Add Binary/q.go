package main

import "fmt"

func addBinary(a string, b string) string {
	mx, mn := MaxStringLen(a, b)
	var mind rune = 0
	i := len(mx) - 1
	for ;i >= 0; i--{
		mnIndex := i - len(mx) + len(mn)
		var value rune = 0
		if mnIndex >= 0{
			value = mn[mnIndex] - '0'
		}
		value = mx[i] - '0' + value + mind
		if value > 1{
			mind = 1
			value%=2
		} else {
			mind = 0
		} 
		mx[i] = value + '0'
	}
	if mind == 1{
		mx = append([]rune{'1'}, mx...)
	}
	return string(mx)
}

func MaxStringLen(a, b string) ([]rune, []rune){
	if len(a) > len(b) {
		return []rune(a), []rune(b)
	}
	return []rune(b), []rune(a)
}

func main(){
	fmt.Println(addBinary("11", "1"))
}