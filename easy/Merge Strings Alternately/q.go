package main

func mergeAlternately(word1 string, word2 string) string {
	res := make([]byte, 0, len(word1) + len(word2))
	mx := len(word1)
	if mx < len(word2){
		mx = len(word2)
	}
	for i := 0; i < mx; i++{
		if i < len(word1){
			res = append(res, word1[i])
		}
		if i < len(word2){
			res = append(res, word2[i])
		}
	}
	return string(res)
}