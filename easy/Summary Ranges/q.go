package main

import (
	"fmt"
	"strconv"
)

func summaryRanges(nums []int) []string {
	if len(nums) == 0 {
		return []string{}
	}
	res := []string{}
    start := nums[0]
	end := nums[0]
	for i := 1; i < len(nums); i++{
		n := nums[i]
		if n - nums[i - 1] == 1 {
			end = n
			continue
		}
		if start != end{
			r := fmt.Sprintf("%d->%d", start, end)
			res = append(res, r)
		} else {
			res = append(res, strconv.Itoa(start))
		}
		start = n
		end = n
	}
	if start != end{
		r := fmt.Sprintf("%d->%d", start, end)
		res = append(res, r)
	} else {
		res = append(res, strconv.Itoa(start))
	}
	return res
}