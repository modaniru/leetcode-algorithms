package main

func peakIndexInMountainArray(arr []int) int {
    for left, right := 0, len(arr); left < right;{
		middle := (left + right) / 2
		if arr[middle] > arr[middle + 1] && arr[middle] > arr[middle - 1]{
			return middle
		} else if (arr[middle] > arr[middle + 1]){
			right = middle
		} else {
			left = middle
		}
	}
	return -1
}

